# A simple docker-compose deployment of Trellis LDP

## Running

1. Install Docker and Docker Compose.
2. Start the service by running `docker-compose up` in the root of this folder. Depending on your configuration you might have to use `sudo`.
3. Find the Trellis endpoint at http://localhost/, the Blazegraph UI at http://localhost:9999, as well as the Blazegraph SPARQL endpoint (DO NOT DIRECTLY UPDATE THROUGH THIS ENPOINT - ONLY USE THE TRELLIS API!!!) at http://localhost:9999/blazegraph/namespace/kb/sparql

